Spree::Core::Engine.routes.draw do
  namespace :admin do
    resources :products do
      resources :product_notes, only: [:index, :create]      
    end
  end
end
