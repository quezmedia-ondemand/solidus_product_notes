module Spree
  class ProductNote < ActiveRecord::Base
    belongs_to :noteable, polymorphic: true
    belongs_to :user, required: true, class_name: Spree.user_class

    # A note can have replies which are essentially notes
    has_many :replies, as: :noteable, class_name: Spree::ProductNote,
      foreign_key: 'noteable_id'

    validates :body, presence: true
  end
end
