module Spree
  Product.class_eval do
    has_many :notes,              class_name: Spree::ProductNote,
                                  foreign_key: 'noteable_id',
                                  dependent: :destroy
  end
end
