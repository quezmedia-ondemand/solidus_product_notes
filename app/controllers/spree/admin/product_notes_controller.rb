module Spree
  module Admin
    class ProductNotesController < Spree::Admin::BaseController
      before_action :find_product

      def index
        @notes = @product.notes
        @note = Spree::ProductNote.new(product_note_init_params)
      end

      def create
        @note = Spree::ProductNote.new(product_note_params)

        if @note.save
          respond_with(@note) do |format|
            format.html { redirect_to(
              admin_product_product_notes_path(product_id: @product.id),
              success: Spree.t(:created_product_note))
            }
          end
        else
          render :index
        end
      end

      private
      def find_product
        @product = Spree::Product.friendly.find(params[:product_id])
      end

      def product_note_init_params
        {
          user_id: try_spree_current_user.try(:id),
          noteable_id: @product.id,
          noteable_type: @product.class
        }
      end

      def product_note_params
        params.require(:product_note).permit(:user_id, :body, :noteable_id, :noteable_type)
      end
    end
  end
end
