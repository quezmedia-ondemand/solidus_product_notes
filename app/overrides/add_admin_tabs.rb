Deface::Override.new(:virtual_path => "spree/admin/shared/_product_tabs",
                     :name => "product_notes_admin_product_tabs",
                     :insert_bottom => "[data-hook='admin_product_tabs']",
                     :partial => "spree/admin/shared/product_notes_product_tabs",
                     :original => '6ff701437e1cbbb391a9f902c5a9bbfa1b448ce6',
                     :disabled => false)
