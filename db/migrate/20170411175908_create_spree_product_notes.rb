class CreateSpreeProductNotes < ActiveRecord::Migration[5.0]
  def change
    create_table :spree_product_notes do |t|
      t.text :body
      t.references :noteable, polymorphic: true, index: true
      t.references :user, index: true, foreign_key: { to_table: :spree_users }

      t.timestamps
    end
  end
end
